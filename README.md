# recipe-app-api-proxy

NGINX Proxy App for our recipe app API

## Usage

### Environment Variables

* 'LISTEN_PORT' -  Port to Listen On (default: '8000')
* 'APP_HOST'    -  Hostname of the app to forward requests to (default: 'app')
* 'APP_PORT'    -  Port of the app to forward requests to (default: '9000')
